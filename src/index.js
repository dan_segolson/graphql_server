const { GraphQLServer } = require('graphql-yoga')
const Query = require('./resolvers/Query')
const Mutation = require('./resolvers/Mutations')

const resolvers = {
  Query,
  Mutation,  
}

console.log(resolvers)

const server = new GraphQLServer({
  typeDefs: './src/schemas/hov_rest_schema.graphql',
  resolvers,
})

server.start(() => console.log(`GQL-node-Server is running on http://localhost:4000`))