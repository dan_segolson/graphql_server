const c = require('../utils/Constants')

//console.log(`${c.hovURL}${c.login}`);
login = (parent, args, context) => {
	const body = '{"sUsername":"'+args.username+'","sPassword":"'+args.password+'","sAppToken":"'+args.app_token+'"}'
	//console.log(body)

	return c.fetch(`${c.hovURL}${c.login}`, {
			method: 'post',
	        body:    body,
	        headers: { 'Content-Type': 'application/json' },
	    }).then(res => res.json())
}

logout = (parent, args, context) => {
	const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'"}'
	//console.log(body)

	return c.fetch(`${c.hovURL}${c.logout}`, {
			method: 'post',
	        body:    body,
	        headers: { 'Content-Type': 'application/json' },
	    }).then(res => res.json())
}

stage_login = (parent, args, context) => {
	const body = '{"sUsername":"'+args.username+'","sPassword":"'+args.password+'","sAppToken":"'+args.app_token+'"}'
	//console.log(body)

	return c.fetch(`${c.hovStageURL}${c.stage_login}`, {
			method: 'post',
	        body:    body,
	        headers: { 'Content-Type': 'application/json' },
	    }).then(res => res.json()) //.then(res => console.log(res));
}

stage_logout = (parent, args, context) => {
	const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'"}'
	//console.log(body)

	return c.fetch(`${c.hovStageURL}${c.stage_logout}`, {
			method: 'post',
	        body:    body,
	        headers: { 'Content-Type': 'application/json' },
	    }).then(res => res.json())
}


module.exports = {
  login,
  logout,
  stage_login,
  stage_logout,

}