const c = require('../utils/Constants')

//console.log(`${c.hovStageURL}${c.rewards}`);
rewards = () => {
  console.log('calling rewards');
	return c.fetch(`${c.hovStageURL}${c.rewards}`).then(res => res.json()) //.then(res => console.log(res));
}

stage_player_ratings = (parent, args, context) => {
  const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'", "iCharId" : "'+args.char_id+'", "sLbType" : "'+args.lb_type+'", "iLimit" : "'+args.limit+'","iYear": "'+args.year+'","iMonth" : "'+args.month+'"}'
  //console.log(body)

  return c.fetch(`${c.hovStageURL}${c.leaderboard}`, {
      method: 'post',
          body:    body,
          headers: { 'Content-Type': 'application/json' },
      }).then(res => res.json())
}

stage_all_characters = (parent, args, context) => {
  const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'"}' 

  return c.fetch(`${c.hovStageURL}${c.all_chars}`, {
      method: 'post',
          body:    body,
          headers: { 'Content-Type': 'application/json' },
      }).then(res => res.json())
}

stage_all_events = (parent, args, context) => {
  const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'"}' 
  return c.fetch(`${c.hovStageURL}${c.all_events}`, {
      method: 'post',
          body:    body,
          headers: { 'Content-Type': 'application/json' },
      }).then(res => res.json()); //.then(data => console.log(data)).catch(error => console.error(error));
}

stage_player_progress = (parent, args, context) => {
  const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'"}' 
  return c.fetch(`${c.hovStageURL}${c.player_progress}`, {
      method: 'post',
          body:    body,
          headers: { 'Content-Type': 'application/json' },
      }).then(res => res.json());//.then(data => console.log(data)).catch(error => console.error(error));
}

stage_game_rewards = (parent, args, context) => {
  const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'"}'
  //console.log(body)

  return c.fetch(`${c.hovStageURL}${c.game_rewards}`, {
      method: 'post',
          body:    body,
          headers: { 'Content-Type': 'application/json' },
      }).then(res => res.json()) //.then(res => console.log(res));
}

login_status = (parent, args, context) => {
  const body = '{"iPlayerId":"'+args.player_id+'","sLoginToken":"'+args.user_token+'","sAppToken":"'+args.app_token+'"}'
  //console.log(body)

  return c.fetch(`${c.hovStageURL}${c.game_rewards}`, {
      method: 'post',
          body:    body,
          headers: { 'Content-Type': 'application/json' },
      }).then(res => res.json()) //.then(res => console.log(res));
}

module.exports = {
  rewards,
  stage_player_ratings,
  stage_all_characters,
  stage_game_rewards,
  login_status,
  stage_all_events,
  stage_player_progress,
}