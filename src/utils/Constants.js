const fetch = require('node-fetch')
const request = require('request') 
const baseURL = `http://localhost:3000`
const hovURL = `http://aftermath.awave.site/frontend/web-api.php?method=`
//const hovURL = `http://localhost/aftermath_engagement_engine/frontend/web-api/v1`

const rewards = 'api_testApi'
//const rewards = '/rewards'
const login = '/api_login'
const logout = '/api_logout'

const hovStageURL = `https://stage.handsofvictory.com/api/?method=`
//const hovStageURL = `http://192.168.1.117/gameweb/webfront/api/web-api.php?method=`
const stage_login = 'user_logInPlayer'
const stage_logout = 'user_logOutPlayer'
const leaderboard = 'lb_getLeaderboard'
const game_rewards = 'game_getRewards'
const all_chars = 'char_getAllCharacters'
const all_events = 'event_getEvents'
const player_progress = 'user_getPlayerProgress'

module.exports = {
    request: request,
    fetch: fetch,
    baseURL: baseURL,
    hovURL: hovURL,
    hovStageURL: hovStageURL,
    rewards: rewards,
    login: login,
    logout: logout,
    stage_login: stage_login,
    stage_logout: stage_logout,
    leaderboard: leaderboard,
    game_rewards: game_rewards,
    all_chars: all_chars,
    all_events: all_events,
    player_progress: player_progress,
};